import {Form,Button} from 'react-bootstrap'
import {useState, useEffect,useContext} from 'react'
import UserContext from '../UserContext'
import {Redirect,useParams,useHistory} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function EditProduct(){
	const {user, setUser} = useContext(UserContext)
	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const {productId} =useParams()
	const [isActive, setIsActive] = useState(false)
	const history = useHistory()
	useEffect(() =>{

		fetch(`https://fathomless-sands-52112.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data =>{
			
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	},[productId])


	function Edit(char){
		char.preventDefault();

		fetch(`https://fathomless-sands-52112.herokuapp.com/products/${productId}/update`,{
			method: 'POST',
			headers:{
				'Content-Type' :'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name : name,
				description : description,
				price : price
			})
		})

		.then(res =>res.json())
		.then(data =>{
			console.log(data)
			if(data === true){
				setName('')
				setDescription('')
				setPrice('')
			Swal.fire({
					title:'Edit Product',
					icon:'succes',
					text: 'Nice'
				})
			history.push("/aProduct")
		} 
		else {
				Swal.fire({
					title:'Some inputs are invalid.',
					icon:'error',
					text: 'Check your details'
				})
			}	
		})
		
	}
	
	return(
			(!user.isAdmin)?
			<Redirect to='/activeProduct'/>
			:
			<Form onSubmit= {(char)=>Edit(char)}>
				<Form.Group>
					<Form.Label>Name of product:</Form.Label>
					<Form.Control
					type= 'string'
					placeholder = 'Product name'
					required
					value = {name}
					onChange = {char => setName(char.target.value)}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Description</Form.Label>
					<Form.Control
					type= 'string'
					placeholder = 'Describe your product'
					required
					value = {description}
					onChange = {char => setDescription(char.target.value)}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Price :</Form.Label>
					<Form.Control
					type= 'number'
					placeholder = 'Set your product price'
					required
					value = {price}
					onChange = {char => setPrice(char.target.value)}
					/>
				</Form.Group>
				
					<Button variant = 'primary' type = 'submit' id= 'submitBtn'>
					Edit
					</Button>
					
				
				
			</Form>

		)
}