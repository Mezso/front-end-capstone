import {Form,Button} from 'react-bootstrap'
import {useState, useEffect,useContext} from 'react'
import UserContext from '../UserContext'
import {Redirect , useHistory} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function Register(){
	const {user, setUser} = useContext(UserContext)
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [email, setEmail] = useState('')
	const [firstName, setFname] = useState('')
	const [lastName, setLname] = useState('')
	const [mobileNo, setMobile] = useState('')
	const [isActive, setIsActive] = useState(false)

	const history = useHistory()
	useEffect(() =>{
		if((email !== '' && password1!=='' && password2 !== '' && firstName !== '' && lastName !== '' && mobileNo !== '' ) && (password1===password2)){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	},[email,password1,password2,firstName,lastName,mobileNo])

	function registerUser(char){
		char.preventDefault();

		fetch('https://fathomless-sands-52112.herokuapp.com/users/register',{
			method: 'POST',
			headers:{
				'Content-Type' :'application/json'
			},
			body: JSON.stringify({
				firstName : firstName,
				lastName : lastName,
				email : email,
				mobileNo : mobileNo,
				password : password1
			})
		})

		.then(res =>res.json())
		.then(data =>{

			console.log(data)
			if(data === true){
				setEmail('')
				setPassword1('')
				setPassword2('')
				setFname('')
				setLname('')
				setMobile('')
			Swal.fire({
					title:'Account Successfully registed',
					icon:'succes',
					text: 'Proceed to login'
				})
			
		} 
		else {
				Swal.fire({
					title:'Some inputs are invalid.',
					icon:'error',
					text: 'Check your details'
				})
			}	
		})

		
	}
	return(
			(user.id !== null)?
			<Redirect to = "/login"/>:
			<Form onSubmit= {(char)=>registerUser(char)}>
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control
					type= 'string'
					placeholder = 'Please enter your First name here'
					required
					value = {firstName}
					onChange = {char => setFname(char.target.value)}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control
					type= 'string'
					placeholder = 'Please enter your Last name here'
					required
					value = {lastName}
					onChange = {char => setLname(char.target.value)}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile number:</Form.Label>
					<Form.Control
					type= 'number'
					placeholder = 'Please enter your Mobile number here'
					required
					value = {mobileNo}
					onChange = {char => setMobile(char.target.value)}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email Address:</Form.Label>
					<Form.Control
					type= 'email'
					placeholder = 'Please enter your email here'
					required
					value = {email}
					onChange = {char => setEmail(char.target.value)}
					/>

				</Form.Group>
				<Form.Group controlId = "password1">
				<Form.Label>Password:</Form.Label>
				<Form.Control
				type = 'password'
				placeholder = 'Please input your password'
				value = {password1}
				onChange = {char => setPassword1(char.target.value) }
				required

				/>
				</Form.Group>
				<Form.Group controlId= 'password2'>
				<Form.Label>Verify password :</Form.Label>
				<Form.Control
				type = 'password'
				placeholder = 'Please verify your password'
				value = {password2}
				onChange = {char =>setPassword2(char.target.value)}
				required

				/>
				</Form.Group>
				{ isActive ?
					<Button variant = 'primary' type = 'submit' id= 'submitBtn'>
					Register
					</Button>
					:
					<Button variant = 'danger' type = 'submit' id= 'submitBtn' disabled>
					Register
					</Button>
				}
				
			</Form>

		)
}