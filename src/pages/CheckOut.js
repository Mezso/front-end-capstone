import {useState, useEffect,useContext, Fragment} from 'react'
import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import {useParams, useHistory, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'



export default function CourseView() {
	const {productId} =useParams()
	const {user} = useContext(UserContext)


	const history = useHistory()

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)
	const [isActive, setisActive] = useState(true)

	useEffect(() =>{

		fetch(`https://fathomless-sands-52112.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data =>{
			
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setisActive(data.isActive)
		})
	},[productId])
 		
 		const add = (productId) =>{
 			fetch(`https://fathomless-sands-52112.herokuapp.com/orders/addCart`,{
 				method: 'POST',
 				headers: {
 					"Content-Type" : "application/json",
 					Authorization : `Bearer ${localStorage.getItem('token')}`
 				},
 				body: JSON.stringify({
 					
 					userId: user.id,
 					productId: productId
 				})
 			})
 			.then(res => res.json())
 			.then(data =>{
 				if(data === true){
 				Swal.fire({
 					title:'Add to cart!',
 					icon: 'success',
 					text: 'Added to your cart'
 				})
 				history.push("/activeProduct")
 			}
 				else{
 					Swal.fire({
 					title:'Something went wrong!',
 					icon: 'Error',
 					text: 'Please try again!'
 				})
 			}
 			})
 			
 			
 		}
 		const archive = (productId) =>{
		fetch(`https://fathomless-sands-52112.herokuapp.com/products/${productId}/archive`,{
			method: 'PUT',
			headers:{
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res =>res.json())
		.then(data =>{
			if(data === true){
 				Swal.fire({
 					title:'Archive',
 					icon: 'success',
 					text: 'Archive the product'
 				})
 				history.push("/aProduct")
 			}
 				else{
 					Swal.fire({
 					title:'Something went wrong!',
 					icon: 'Error',
 					text: 'Please try again!'
 				})
 			}
		})
	}
	const activate = (productId) =>{
		fetch(`https://fathomless-sands-52112.herokuapp.com/products/${productId}/activate`,{
			method: 'PUT',
			headers:{
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res =>res.json())
		.then(data =>{
			if(data === true){
 				Swal.fire({
 					title:'Activate',
 					icon: 'success',
 					text: 'Archive the product'
 				})
 				history.push("/aProduct")
 			}
 				else{
 					Swal.fire({
 					title:'Something went wrong!',
 					icon: 'Error',
 					text: 'Please try again!'
 				})
 			}
		})
	}

	return (

			<Container className = "mt-5">
					<Row>
							<Col lg = {{span:6,offset:3}}>
							<Card>
									<Card.Body className = "text-center">
											<Card.Title>{name}</Card.Title>
											<Card.Subtitle>Description:</Card.Subtitle>
											<Card.Text>{description}</Card.Text>
											<Card.Subtitle>Price:</Card.Subtitle>
											<Card.Text>PHP {price}</Card.Text>
											{
												isActive?
												<Card.Text>Item is available</Card.Text>:
												<Card.Text>Item is not available</Card.Text>
											}
											{user.isAdmin?
											
												<Fragment>
												<Button variant = "primary" className="mx-2" onClick = {() => archive(productId)}>Archive</Button>
												<Button variant = "primary" className="mx-2" onClick = {() => activate(productId)}>Activate</Button>
												<Link className ="btn btn-primary btn-block mx-2" to ={`/products/${productId}/update`}>Edit</Link>
												</Fragment>
												:
											
												user.id !== null?
													<Button variant = "primary" onClick = {() => add(productId)}>Add to cart</Button>
													:
													<Link className ="btn btn-danger btn-block" to ="/login">Login first</Link>
												
											}
											

									</Card.Body>
							</Card>
							</Col>
					</Row>
			</Container>

		)
}