import {Form,Button} from 'react-bootstrap'
import {useState, useEffect,useContext} from 'react'
import UserContext from '../UserContext'
import {Redirect} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function CreateProduct(){
	const {user, setUser} = useContext(UserContext)
	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')

	const [isActive, setIsActive] = useState(false)
	useEffect(() =>{
		if(name !== '' && description!=='' && price !== '' ){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	},[name,description,price])

	function registerProduct(char){
		char.preventDefault();

		fetch('https://fathomless-sands-52112.herokuapp.com/products/create',{
			method: 'POST',
			headers:{
				'Content-Type' :'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name : name,
				description : description,
				price : price
			})
		})

		.then(res =>res.json())
		.then(data =>{
			console.log(data)
			if(data === true){
				setName('')
				setDescription('')
				setPrice('')
			Swal.fire({
					title:'New product added',
					icon:'succes',
					text: 'Thank you'
				})
		} 
		else {
				Swal.fire({
					title:'Some inputs are invalid.',
					icon:'error',
					text: 'Check your details'
				})
			}	
		})
		
	}
	
	return(
			(!user.isAdmin)?
			<Redirect to='/activeProduct'/>
			:
			<Form onSubmit= {(char)=>registerProduct(char)}>
				<Form.Group>
					<Form.Label>Name of product:</Form.Label>
					<Form.Control
					type= 'string'
					placeholder = 'Product name'
					required
					value = {name}
					onChange = {char => setName(char.target.value)}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Description</Form.Label>
					<Form.Control
					type= 'string'
					placeholder = 'Describe your product'
					required
					value = {description}
					onChange = {char => setDescription(char.target.value)}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Price :</Form.Label>
					<Form.Control
					type= 'number'
					placeholder = 'Set your product price'
					required
					value = {price}
					onChange = {char => setPrice(char.target.value)}
					/>
				</Form.Group>
				{ isActive ?
					<Button variant = 'primary' type = 'submit' id= 'submitBtn'>
					Create
					</Button>
					:
					<Button variant = 'danger' type = 'submit' id= 'submitBtn' disabled>
					Create
					</Button>
				}
				
			</Form>

		)
}