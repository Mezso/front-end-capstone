import {Fragment, useEffect, useState} from 'react'
// import coursesData from '../data/coursesData'
import AdminCard from '../components/AdminCard'
export default function CProduct(){
	// console.log(coursesData)
	// console.log(coursesData[0])
const [product, setProduct] = useState([])
useEffect(() =>{
		fetch('https://fathomless-sands-52112.herokuapp.com/users/aProduct',{
			headers:{
				'Content-Type' :'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data=>{
			console.log(data)
			setProduct(data.map(product =>{
				return (
					<AdminCard key = {product.id} AdminProp = {product}/>
					)
			}))
		})
},[])


	return(
		<Fragment>
				<h1>Products</h1>
				{product}
		</Fragment>
	)
}