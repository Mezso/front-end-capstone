import {Fragment} from 'react'
import Banner from '../components/Banner'



export default function home (){

	const data = {
		title: "Welcome to CSJ SHOP",
		content: "Where you can by anything you want, that helps you to fulfill your imagination",
		destination: "/activeProduct",
		label: "Shop us now"
	}
	return(
		<Fragment>
				<Banner data = {data}/>
				
		</Fragment>
		)
}