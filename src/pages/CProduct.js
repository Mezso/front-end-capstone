import {Fragment, useEffect, useState} from 'react'
// import coursesData from '../data/coursesData'
import ProductsCard from '../components/ProductsCard'

export default function CProduct(){
	// console.log(coursesData)
	// console.log(coursesData[0])
const [product, setProduct] = useState([])
useEffect(() =>{
		fetch('https://fathomless-sands-52112.herokuapp.com/products/activeProduct')
		.then(res => res.json())
		.then(data=>{

			setProduct(data.map(product =>{
				return (
					<ProductsCard key = {product.id} productProp = {product}/>
					)
			}))
			console.log(data)
		})
},[])


	return(
		<Fragment>
				<h1>Products</h1>
				{product}

		</Fragment>
	)
}