import {useState,useEffect} from 'react'
// import {Fragment} from 'react'
import {Container} from 'react-bootstrap'

import AppNavbar from './components/AppNavbar'

import Register from './pages/Register'
import CreateProduct from './pages/CreateProduct'
import Login from './pages/Login'
import Logout from './pages/Logout'
import CProduct from './pages/CProduct'
import CheckOut from './pages/CheckOut'
import AdminProduct from './pages/AdminProduct'
import EditProduct from './pages/EditProduct'
import Cart from './pages/Cart'
import Home from './pages/Home'
import Error from './pages/Error';

import {UserProvider} from './UserContext'



import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'


import './App.css';

function App() {
		const [user,setUser]=useState({
		id:null,
		isAdmin:null
	})
	const unsetUser = () =>{
		localStorage.clear()
	}

	useEffect(() =>{
		let token = localStorage.getItem('token')
		fetch('https://fathomless-sands-52112.herokuapp.com/users/details',{
			method : "GET",
			headers:{
				"Authorization": `Bearer ${token}`
			}
		})
		.then(res=> res.json())
		.then(data =>{
			if(typeof data._id !== "undefined"){
				setUser({
					id:data._id,
					isAdmin : data.isAdmin
				})
			}
			else {
				setUser({
					id:null,
					isAdmin:null
				})
			}
		})
	},[user])

		return (
			<UserProvider value={{user,setUser,unsetUser}}>
			<Router>

				<AppNavbar/>
				<Container>
					<Switch>
					<Route exact path="/register" component = {Register}/>
					<Route exact path="/CreateProduct" component = {CreateProduct}/>
					<Route exact path="/login" component = {Login}/>
					<Route exact path="/activeProduct" component = {CProduct}/>
					<Route exact path="/aProduct" component = {AdminProduct}/>
					<Route exact path="/products/:productId" component = {CheckOut}/>
					<Route exact path="/products/:productId/update" component = {EditProduct}/>
					<Route exact path="/logout" component = {Logout}/>
					<Route exact path="/order" component = {Cart}/>
					<Route exact path="/" component = {Home}/>
					<Route exact path="*" component = {Error}/>
					</Switch>
				</Container>
			</Router>
			</UserProvider>

		)
}

export default App;
