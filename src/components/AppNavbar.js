// import Navbar from 'react-bootstrap/Navbar'
// import Nav from 'react-bootstrap/Nav'

import {Navbar , Nav} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import {Fragment, useContext} from 'react'
import UserContext from '../UserContext'

export default function AppNavbar(){
	const {user} =useContext(UserContext)
	return(

		<Navbar bg="light" expand="lg">
		<Navbar.Brand as={Link} to = "/">CSJ SHOP</Navbar.Brand>
		<Navbar.Toggle aria-controls="basic-navbar-nav" />
		<Navbar.Collapse id="basic-navbar-nav">
		<Nav className="mr-auto">
		<Nav.Link as={Link} to ="/" exact>Home</Nav.Link>

		{
			(user.isAdmin)?
			<Fragment>
			<Nav.Link as={Link} to ="/aProduct" exact>Products</Nav.Link>
			<Nav.Link as={Link} to ="/CreateProduct" exact>Create Products</Nav.Link>
			</Fragment>
			:
			<Fragment>
			<Nav.Link as={Link} to ="/activeProduct" exact>Products</Nav.Link>
			
			</Fragment>
		}
			
		{	(user.id !== null)?
			<Fragment>
			<Nav.Link as={Link} to ="/order" exact>Order History</Nav.Link>
			<Nav.Link as={Link} to ="/logout" exact>Logout</Nav.Link>
			
			</Fragment>
			:
			<Fragment>
			<Nav.Link as={Link} to ="/register" exact>Register</Nav.Link>
			<Nav.Link as={Link} to ="/login" exact>Login</Nav.Link>
			</Fragment>
		}



		
		
		</Nav>
		</Navbar.Collapse>
		</Navbar>

		)
}