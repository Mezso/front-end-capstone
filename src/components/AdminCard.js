import {useState} from 'react'
import {Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function AdminCard({AdminProp}){
	// console.log(courseProp)
	const {name, description, price, _id} = AdminProp
	const [count,setCount] = useState(0)
		return(

			<Card >
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>{price}</Card.Text>
{/*					<Card.Text>Enrollees:{count} Seat: {seat}</Card.Text>*/}
					<Link className ="btn btn-primary" to={`/products/${_id}`}>details</Link>
				</Card.Body>
			</Card>
	
	
	)
}