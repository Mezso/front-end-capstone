import {useState, useEffect,useContext, Fragment} from 'react'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

import {Card, Button, Col, Row} from 'react-bootstrap'
import {Link,useHistory} from 'react-router-dom'

export default function ProductsCard({productProp}){
	// console.log(courseProp)
	const {name, description, price,isActive, _id} = productProp
	const [cart,setCart] = useState([])
	const [page,setPage] = useState('products')
	const {user} = useContext(UserContext)
	const history = useHistory()
	const [qty,setCount] = useState(1)
	const [dqty,setDCount] = useState(0)


	function quantity (){
		setCount(qty +1)
	}
	function dquantity(){
		if(qty > 0){
			setCount(qty -1)
		}
		
	}

	const checkout = (productProp) =>{
	
		fetch(`https://fathomless-sands-52112.herokuapp.com/orders/addCart`,{
 				method: 'POST',
 				headers: {
 					"Content-Type" : "application/json",
 					Authorization : `Bearer ${localStorage.getItem('token')}`
 				},
 				body: JSON.stringify({
 					
 					userId: user.id,
 					productId: productProp._id
 				})
 			})
 			.then(res => res.json())
 			.then(data =>{
 				if(data === true){
 				Swal.fire({
 					title:'Add to cart!',
 					icon: 'success',
 					text: 'Added to your cart'
 				})
 				setCount(1)
 				history.push("/activeProduct")
 			}
 				else{
 					Swal.fire({
 					title:'Something went wrong!',
 					icon: 'Error',
 					text: 'Please try again!'
 				})
 			}
 			})

}
const renderProduct = () =>(
		<>
			<Card  >
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>{price}</Card.Text>
					{
						isActive?
						<Card.Text>Item is available</Card.Text>:
						<Card.Text>Item is not available</Card.Text>

					}
					<Card.Text>Subtotal : {price*qty}</Card.Text>
					<Card.Subtitle>Qty:{qty}</Card.Subtitle>
					<Fragment>
					<Button variant = "primary" className="mx-2 mt-2" onClick = {() => quantity()}>+</Button>
					<Button variant = "primary" className="mx-2 mt-2" onClick = {() => dquantity()}>-</Button>
					
					</Fragment>
					
					{
						user.id !== null?
						<Button variant = "primary" className="mx-2 mt-2" onClick = {() => checkout(productProp)}>Check out</Button>
						:
						<Link className ="btn btn-danger btn-block mx-2 mt-2" to ="/login">Login first</Link>
					}
					{/*<Link className ="btn btn-primary" to={`/products/${_id}`}>details</Link>*/}
				</Card.Body>
			</Card>


		</>

)


		return(

			renderProduct()
	
	)
}