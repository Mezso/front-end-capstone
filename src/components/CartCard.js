import {Fragment, useEffect, useState} from 'react'
import {Card, Button, Form, Row,Container} from 'react-bootstrap'
import {useParams, useHistory, Link} from 'react-router-dom'

export default function CartCard({cartProp}){
	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)
	const [qty,setCount] = useState(0)
	const [dqty,setDCount] = useState(0)
	const  {productId,purchasedOn,_id} = cartProp
	console.log(purchasedOn)
	useEffect(() =>{

		fetch(`https://fathomless-sands-52112.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data =>{
			
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			
		})
	},[])
	


		return(

			<Card >
				<Card.Body>
					<Card.Subtitle>Order Id: {_id}</Card.Subtitle>
					<Card.Subtitle>Date: {purchasedOn}</Card.Subtitle>
					<Card.Title>{name}</Card.Title>
					
					

				</Card.Body>
			</Card>
	
	
	)
}