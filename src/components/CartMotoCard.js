import {useState, useEffect,useContext, Fragment} from 'react'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

import {Card, Button, Col, Row} from 'react-bootstrap'
import {Link,useHistory} from 'react-router-dom'

export default function CartMotoCard(CartProp){
	// console.log(courseProp)

	
	const {name, description, price,isActive, _id}=CartProp.data
	
	const [cart,setCart] = useState([])
	const [page,setPage] = useState('products')
	const {user} = useContext(UserContext)
	const history = useHistory()

	
	console.log(CartProp.data.name)
	const addToCart = (CartProp) =>{
	
		// fetch(`https://fathomless-sands-52112.herokuapp.com/orders/addCart`,{
 	// 			method: 'POST',
 	// 			headers: {
 	// 				"Content-Type" : "application/json",
 	// 				Authorization : `Bearer ${localStorage.getItem('token')}`
 	// 			},
 	// 			body: JSON.stringify({
 					
 	// 				userId: user.id,
 	// 				productId: CartProp._id
 	// 			})
 	// 		})
 	// 		.then(res => res.json())
 	// 		.then(data =>{
 	// 			if(data === true){
 	// 			Swal.fire({
 	// 				title:'Add to cart!',
 	// 				icon: 'success',
 	// 				text: 'Added to your cart'
 	// 			})
 	// 			history.push("/activeProduct")
 	// 		}
 	// 			else{
 	// 				Swal.fire({
 	// 				title:'Something went wrong!',
 	// 				icon: 'Error',
 	// 				text: 'Please try again!'
 	// 			})
 	// 		}
 	// 		})

}
const renderProduct = () =>(
		<>
			<Card  >
				<Card.Body>
					<Card.Title>{cart.name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{cart.description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>{cart.price}</Card.Text>
					{
						isActive?
						<Card.Text>Item is available</Card.Text>:
						<Card.Text>Item is not available</Card.Text>

					}
					
					<Button variant = "primary" className="mx-2" onClick = {() => addToCart(CartProp)}>Add to Cart</Button>
					{/*<Link className ="btn btn-primary" to={`/products/${_id}`}>details</Link>*/}
				</Card.Body>
			</Card>

		</>

)


		return(

			
			renderProduct()
	
	)
}